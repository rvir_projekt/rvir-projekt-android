
import { StyleSheet, Text, View, Image, Button, Dimensions, ScrollView, Pressable } from 'react-native';
import 'expo-dev-client';
import { GoogleSignin, GoogleSigninButton } from '@react-native-google-signin/google-signin';
import auth from '@react-native-firebase/auth';
import React, { useState, useEffect } from 'react';
import { LineChart, BarChart, PieChart, ProgressChart, ContributionGraph, StackedBarChart } from "react-native-chart-kit";
import { getLocales } from 'expo-localization';
import DeviceCountry, { TYPE_ANY, TYPE_TELEPHONY, TYPE_CONFIGURATION } from 'react-native-device-country';
import FlashMessage, { showMessage } from "react-native-flash-message";
import { Overlay, Icon } from '@rneui/themed';
import { Dropdown, SelectCountry } from 'react-native-element-dropdown';
import Ionicons from '@expo/vector-icons/Ionicons';
import { all_countries } from '../data/countries';
import { numbers } from '../data/numbers';

const data = [
    { label: 'GDP (current US$)', value: 'NY.GDP.MKTP.CD' },
    { label: 'GDP growth (annual %)', value: 'NY.GDP.MKTP.KD.ZG' },
    { label: 'GDP per capita (current US$)', value: 'NY.GDP.PCAP.CD' },
    { label: 'Inflation (annual %)', value: 'NY.GDP.DEFL.KD.ZG' },
    { label: 'Unemployment, total (% of total labor force)', value: 'SL.UEM.TOTL.NE.ZS' },
    { label: 'Population, total', value: 'SP.POP.TOTL' },
    { label: 'Population growth (annual %)', value: 'SP.POP.GROW' },
    { label: 'Agricultural land (% of land area)', value: 'AG.LND.AGRI.ZS' },
    { label: 'Forest area (% of land area)', value: 'AG.LND.FRST.ZS' },
    { label: 'CO2 emissions (metric tons per capita)', value: 'EN.ATM.CO2E.PC' },
    { label: 'Life expectancy at birth, total (years)', value: 'SP.DYN.LE00.IN' },
    { label: 'Individuals using the Internet (% of population)', value: 'IT.NET.USER.ZS' },
    { label: 'Air transport, passengers carried', value: 'IS.AIR.PSGR' },
    //{ label: '', value: '' },
    //{ label: '', value: '' },
    //{ label: '', value: '' },
];

const SignedHome = () => {
    const [initializing, setInitializing] = useState(true);
    const [user, setUser] = useState();
    const [data1, setData1] = useState([0, 0, 0, 0]);
    const [data2, setData2] = useState([0, 0, 0, 0]);
    const [data3, setData3] = useState([0, 0, 0, 0]);
    const [data4, setData4] = useState([0, 0, 0, 0]);
    const [data5, setData5] = useState([0, 0, 0, 0]);
    const [data6, setData6] = useState([0, 0, 0, 0]);
    const [years1, setYears1] = useState([2019, 2020, 2021, 2022]);
    const [years2, setYears2] = useState([2019, 2020, 2021, 2022]);
    const [years3, setYears3] = useState([2019, 2020, 2021, 2022]);
    const [years4, setYears4] = useState([2019, 2020, 2021, 2022]);
    const [years5, setYears5] = useState([2019, 2020, 2021, 2022]);
    const [years6, setYears6] = useState([2019, 2020, 2021, 2022]);
    const [bdpSize, setBdpSize] = useState(15);
    const [country, setCountry] = useState('US');
    const [countryName, setCountryName] = useState('USA');
    const [indicators, setIndicators] = useState([data[0].value, data[0].value, data[0].value, data[0].value, data[0].value, data[0].value]);
    const [indicator1, setIndicator1] = useState('');
    const [indicator2, setIndicator2] = useState('');
    const [indicator3, setIndicator3] = useState('');
    const [indicator4, setIndicator4] = useState('');
    const [indicator5, setIndicator5] = useState('');
    const [indicator6, setIndicator6] = useState('');
    const [vsiPred, setVsiPred] = useState(['', '', '', '', '', '']);
    const [vsiPo, setVsiPo] = useState(['', '', '', '', '', '']);
    const [numberValue, setNumberValue] = useState(5);
    const [visible, setVisible] = useState(false);
    const [value1, setValue1] = useState(null);
    const [value2, setValue2] = useState(null);
    const [value3, setValue3] = useState(null);
    const [value4, setValue4] = useState(null);
    const [value5, setValue5] = useState(null);
    const [value6, setValue6] = useState(null);
    const [isFocus, setIsFocus] = useState(false);

    const toggleOverlay = () => {
        setVisible(!visible);
    };

    const makeAPICall = async (arr) => {
        try {
            let country_ = country;

            for (let j = 0; j < arr.length; j++) {

                const response = await fetch(
                    'https://api.worldbank.org/v2/en/country/' + country_ + '/indicator/' + arr[j] + '?mrv=' + numberValue + '&format=json', //NY.GDP.MKTP.CD
                );
                const json = await response.json();
                console.log(json[1][0].country.value);
                if (countryName != json[1][0].country.value) {
                    setCountryName(json[1][0].country.value);
                }
                const len = json[1].length;
                if (len >= 9 && len < 10) {
                    setBdpSize(13);
                } else if (len >= 10 && len < 16) {
                    setBdpSize(9);
                } else if (len >= 16 && len < 21) {
                    setBdpSize(7);
                } else if (len >= 21 && len < 25) {
                    setBdpSize(6);
                } else if (len >= 25) {
                    setBdpSize(5);
                } else {
                    setBdpSize(15);
                }
                let bData = [];
                let allYears = [];

                let largest = 0;
                for (let i = 0; i < len; i++) {
                    if (json[1][i].value > largest) {
                        largest = json[1][i].value;
                    }
                    //bData.push(json[1][i].value / 1000000000);
                    //allYears.push(json[1][i].date);
                }

                let pred = '';
                let po = '';
                if (largest > 1000000000) {
                    for (let i = 0; i < len; i++) {
                        bData.push(json[1][i].value / 1000000000);
                        allYears.push(json[1][i].date);
                    }
                    po = 'B';
                } else if (largest > 1000000) {
                    for (let i = 0; i < len; i++) {
                        bData.push(json[1][i].value / 1000000);
                        allYears.push(json[1][i].date);
                    }
                    po = 'M';
                } else if (largest > 1000) {
                    for (let i = 0; i < len; i++) {
                        bData.push(json[1][i].value / 1000);
                        allYears.push(json[1][i].date);
                    }
                    po = 'T';
                } else {
                    for (let i = 0; i < len; i++) {
                        bData.push(json[1][i].value);
                        allYears.push(json[1][i].date);
                    }
                }

                bData.reverse();
                allYears.reverse();

                if (json[1][0].indicator.value.includes('$')) {
                    pred = '$';
                } else if (json[1][0].indicator.value.includes('%')) {
                    po = '%';
                }

                if (j == 0) {
                    setIndicator1(json[1][0].indicator.value);
                    setData1(bData);
                    setYears1(allYears);
                    let pr = vsiPred;
                    pr[j] = pred;
                    let poo = vsiPo;
                    poo[j] = po;
                    setVsiPred(pr);
                    setVsiPo(poo);
                } else if (j == 1) {
                    setIndicator2(json[1][0].indicator.value);
                    setData2(bData);
                    setYears2(allYears);
                    let pr = vsiPred;
                    pr[j] = pred;
                    let poo = vsiPo;
                    poo[j] = po;
                    setVsiPred(pr);
                    setVsiPo(poo);
                } else if (j == 2) {
                    setIndicator3(json[1][0].indicator.value);
                    setData3(bData);
                    setYears3(allYears);
                    let pr = vsiPred;
                    pr[j] = pred;
                    let poo = vsiPo;
                    poo[j] = po;
                    setVsiPred(pr);
                    setVsiPo(poo);
                } else if (j == 3) {
                    setIndicator4(json[1][0].indicator.value);
                    setData4(bData);
                    setYears4(allYears);
                    let pr = vsiPred;
                    pr[j] = pred;
                    let poo = vsiPo;
                    poo[j] = po;
                    setVsiPred(pr);
                    setVsiPo(poo);
                } else if (j == 4) {
                    setIndicator5(json[1][0].indicator.value);
                    setData5(bData);
                    setYears5(allYears);
                    let pr = vsiPred;
                    pr[j] = pred;
                    let poo = vsiPo;
                    poo[j] = po;
                    setVsiPred(pr);
                    setVsiPo(poo);
                } else if (j == 5) {
                    setIndicator6(json[1][0].indicator.value);
                    setData6(bData);
                    setYears6(allYears);
                    let pr = vsiPred;
                    pr[j] = pred;
                    let poo = vsiPo;
                    poo[j] = po;
                    setVsiPred(pr);
                    setVsiPo(poo);
                }
            }
            //return json;
        } catch (error) {
            console.error(error);
        }
    };

    useEffect(() => {
        setCountry('US');
    }, []);

    useEffect(() => {
        makeAPICall(indicators);
    }, [country, indicators, value1, numberValue, value2, value3, value4, value5, value6]);

    return (
        <>
            <View style={{ alignItems: 'center', backgroundColor: '#0D3B66' }}>
                <Text style={styles.naslov}>Showing results for {countryName} </Text>
            </View>

            <View style={{ backgroundColor: '#0D3B66', alignItems: 'center', paddingBottom: 7 }}>
                <Button
                    title="Modify graphs"
                    onPress={toggleOverlay}
                    buttonStyle={styles.button}
                />
            </View>
            <Overlay overlayStyle={styles.overMain} isVisible={visible} onBackdropPress={toggleOverlay}>
                <View style={{ alignItems: 'center' }}>
                    <Text style={styles.textMain}>
                        Modify graphs
                    </Text>
                </View>

                <Text style={styles.textSelector}>
                    Choose how many years you want the graph so show
                </Text>

                <Dropdown
                    style={[styles.dropdown, isFocus && { borderColor: 'blue' }]}
                    placeholderStyle={styles.placeholderStyle}
                    selectedTextStyle={styles.selectedTextStyle}
                    inputSearchStyle={styles.inputSearchStyle}
                    iconStyle={styles.iconStyle}
                    data={numbers}
                    search
                    maxHeight={300}
                    labelField="label"
                    valueField="value"
                    placeholder={!isFocus ? 'Select number' : '...'}
                    searchPlaceholder="Search..."
                    value={value1}
                    onFocus={() => setIsFocus(true)}
                    onBlur={() => setIsFocus(false)}
                    onChange={item => {
                        setNumberValue(item.value);
                        setIsFocus(false);
                    }}
                    renderLeftIcon={() => (
                        <Ionicons name={'keypad'} size={20} color={isFocus ? 'blue' : 'black'} />

                    )}
                />

                <Text style={styles.textSelector}>
                    Select a country to show data for
                </Text>

                <SelectCountry
                    style={styles.dropdown}
                    selectedTextStyle={styles.selectedTextStyle}
                    placeholderStyle={styles.placeholderStyle}
                    imageStyle={styles.imageStyle}
                    inputSearchStyle={styles.inputSearchStyle}
                    iconStyle={styles.iconStyle}
                    search
                    maxHeight={200}
                    value={country}
                    data={all_countries}
                    valueField="value"
                    labelField="lable"
                    imageField="image"
                    placeholder="Select country"
                    searchPlaceholder="Search..."
                    onChange={e => {
                        setCountry(e.value);
                    }}
                />

                <Text style={styles.textSelector}>
                    Select indicator for graph 1
                </Text>

                <Dropdown
                    style={[styles.dropdown, isFocus && { borderColor: 'blue' }]}
                    placeholderStyle={styles.placeholderStyle}
                    selectedTextStyle={styles.selectedTextStyle}
                    inputSearchStyle={styles.inputSearchStyle}
                    iconStyle={styles.iconStyle}
                    data={data}
                    search
                    maxHeight={300}
                    labelField="label"
                    valueField="value"
                    placeholder={!isFocus ? 'Select indicator' : '...'}
                    searchPlaceholder="Search..."
                    value={value1}
                    onFocus={() => setIsFocus(true)}
                    onBlur={() => setIsFocus(false)}
                    onChange={item => {
                        setValue1(item.value);
                        setIsFocus(false);
                        let all = indicators;
                        all[0] = item.value;
                        setIndicators(all);
                    }}
                    renderLeftIcon={() => (
                        <Ionicons name={'bar-chart-outline'} size={20} color={isFocus ? 'blue' : 'black'} />

                    )}
                />

                <Text style={styles.textSelector}>
                    Select indicator for graph 2
                </Text>

                <Dropdown
                    style={[styles.dropdown, isFocus && { borderColor: 'blue' }]}
                    placeholderStyle={styles.placeholderStyle}
                    selectedTextStyle={styles.selectedTextStyle}
                    inputSearchStyle={styles.inputSearchStyle}
                    iconStyle={styles.iconStyle}
                    data={data}
                    search
                    maxHeight={300}
                    labelField="label"
                    valueField="value"
                    placeholder={!isFocus ? 'Select indicator' : '...'}
                    searchPlaceholder="Search..."
                    value={value2}
                    onFocus={() => setIsFocus(true)}
                    onBlur={() => setIsFocus(false)}
                    onChange={item => {
                        setValue2(item.value);
                        setIsFocus(false);
                        let all = indicators;
                        all[1] = item.value;
                        setIndicators(all);
                    }}
                    renderLeftIcon={() => (
                        <Ionicons name={'bar-chart-outline'} size={20} color={isFocus ? 'blue' : 'black'} />

                    )}
                />

                <Text style={styles.textSelector}>
                    Select indicator for graph 3
                </Text>

                <Dropdown
                    style={[styles.dropdown, isFocus && { borderColor: 'blue' }]}
                    placeholderStyle={styles.placeholderStyle}
                    selectedTextStyle={styles.selectedTextStyle}
                    inputSearchStyle={styles.inputSearchStyle}
                    iconStyle={styles.iconStyle}
                    data={data}
                    search
                    maxHeight={300}
                    labelField="label"
                    valueField="value"
                    placeholder={!isFocus ? 'Select indicator' : '...'}
                    searchPlaceholder="Search..."
                    value={value3}
                    onFocus={() => setIsFocus(true)}
                    onBlur={() => setIsFocus(false)}
                    onChange={item => {
                        setValue3(item.value);
                        setIsFocus(false);
                        let all = indicators;
                        all[2] = item.value;
                        setIndicators(all);
                    }}
                    renderLeftIcon={() => (
                        <Ionicons name={'bar-chart-outline'} size={20} color={isFocus ? 'blue' : 'black'} />

                    )}
                />

                <Text style={styles.textSelector}>
                    Select indicator for graph 4
                </Text>

                <Dropdown
                    style={[styles.dropdown, isFocus && { borderColor: 'blue' }]}
                    placeholderStyle={styles.placeholderStyle}
                    selectedTextStyle={styles.selectedTextStyle}
                    inputSearchStyle={styles.inputSearchStyle}
                    iconStyle={styles.iconStyle}
                    data={data}
                    search
                    maxHeight={300}
                    labelField="label"
                    valueField="value"
                    placeholder={!isFocus ? 'Select indicator' : '...'}
                    searchPlaceholder="Search..."
                    value={value4}
                    onFocus={() => setIsFocus(true)}
                    onBlur={() => setIsFocus(false)}
                    onChange={item => {
                        setValue4(item.value);
                        setIsFocus(false);
                        let all = indicators;
                        all[3] = item.value;
                        setIndicators(all);
                    }}
                    renderLeftIcon={() => (
                        <Ionicons name={'bar-chart-outline'} size={20} color={isFocus ? 'blue' : 'black'} />

                    )}
                />

                <Text style={styles.textSelector}>
                    Select indicator for graph 5
                </Text>

                <Dropdown
                    style={[styles.dropdown, isFocus && { borderColor: 'blue' }]}
                    placeholderStyle={styles.placeholderStyle}
                    selectedTextStyle={styles.selectedTextStyle}
                    inputSearchStyle={styles.inputSearchStyle}
                    iconStyle={styles.iconStyle}
                    data={data}
                    search
                    maxHeight={300}
                    labelField="label"
                    valueField="value"
                    placeholder={!isFocus ? 'Select indicator' : '...'}
                    searchPlaceholder="Search..."
                    value={value5}
                    onFocus={() => setIsFocus(true)}
                    onBlur={() => setIsFocus(false)}
                    onChange={item => {
                        setValue5(item.value);
                        setIsFocus(false);
                        let all = indicators;
                        all[4] = item.value;
                        setIndicators(all);
                    }}
                    renderLeftIcon={() => (
                        <Ionicons name={'bar-chart-outline'} size={20} color={isFocus ? 'blue' : 'black'} />

                    )}
                />

                <Text style={styles.textSelector}>
                    Select indicator for graph 6
                </Text>

                <Dropdown
                    style={[styles.dropdown, isFocus && { borderColor: 'blue' }]}
                    placeholderStyle={styles.placeholderStyle}
                    selectedTextStyle={styles.selectedTextStyle}
                    inputSearchStyle={styles.inputSearchStyle}
                    iconStyle={styles.iconStyle}
                    data={data}
                    search
                    maxHeight={300}
                    labelField="label"
                    valueField="value"
                    placeholder={!isFocus ? 'Select indicator' : '...'}
                    searchPlaceholder="Search..."
                    value={value6}
                    onFocus={() => setIsFocus(true)}
                    onBlur={() => setIsFocus(false)}
                    onChange={item => {
                        setValue6(item.value);
                        setIsFocus(false);
                        let all = indicators;
                        all[5] = item.value;
                        setIndicators(all);
                    }}
                    renderLeftIcon={() => (
                        <Ionicons name={'bar-chart-outline'} size={20} color={isFocus ? 'blue' : 'black'} />

                    )}
                />

                <Button
                    icon={
                        <Icon
                            name="wrench"
                            type="font-awesome"
                            color="white"
                            size={25}
                            iconStyle={{ marginRight: 10 }}
                        />
                    }
                    title="Confirm"
                    onPress={toggleOverlay}
                />
            </Overlay>


            <FlashMessage duration={1000} />
            <ScrollView style={styles.scrollView}>

                <View style={styles.container}>
                    <View style={styles.lineStyle} />
                    <Text style={styles.graphText}>Graph shows {indicator1} data</Text>
                    <LineChart
                        data={{
                            labels: years1,
                            datasets: [
                                {
                                    data: data1
                                }
                            ]
                        }}
                        width={Dimensions.get("window").width} // from react-native
                        height={220}
                        yAxisLabel={vsiPred[0]}
                        yAxisSuffix={vsiPo[0]}
                        yAxisInterval={1} // optional, defaults to 1
                        onDataPointClick={({ value, getColor }) =>
                            showMessage({
                                message: `${vsiPred[0]}${value.toFixed(2)}${vsiPo[0]}`,
                                description: "You selected this value",
                                backgroundColor: "#e26a00"
                            })
                        }
                        chartConfig={{
                            backgroundColor: "#e26a00",
                            backgroundGradientFrom: "#fb8c00",
                            backgroundGradientTo: "#ffa726",
                            decimalPlaces: 1, // optional, defaults to 2dp
                            color: (opacity = 1) => `rgba(255, 255, 255, ${opacity})`,
                            labelColor: (opacity = 1) => `rgba(255, 255, 255, ${opacity})`,
                            propsForVerticalLabels: {
                                fontSize: bdpSize,
                                fontFamily: "roboto-regular",
                            },
                            style: {
                                borderRadius: 16
                            },
                            propsForDots: {
                                r: "6",
                                strokeWidth: "2",
                                stroke: "#D4CBE5"
                            }
                        }}
                        bezier
                        style={{
                            marginVertical: 8,
                            borderRadius: 16
                        }}
                    />
                </View>

                <View style={styles.container}>
                    <View style={styles.lineStyle} />
                    <Text style={styles.graphText}>Graph shows {indicator2} data</Text>
                    <LineChart
                        data={{
                            labels: years2,
                            datasets: [
                                {
                                    data: data2
                                }
                            ]
                        }}
                        width={Dimensions.get("window").width} // from react-native
                        height={220}
                        yAxisLabel={vsiPred[1]}
                        yAxisSuffix={vsiPo[1]}
                        yAxisInterval={1} // optional, defaults to 1
                        onDataPointClick={({ value, getColor }) =>
                            showMessage({
                                message: `${vsiPred[1]}${value.toFixed(2)}${vsiPo[1]}`,
                                description: "You selected this value",
                                backgroundColor: "#e26a00"
                            })
                        }
                        chartConfig={{
                            backgroundColor: "#e26a00",
                            backgroundGradientFrom: "#fb8c00",
                            backgroundGradientTo: "#ffa726",
                            decimalPlaces: 1, // optional, defaults to 2dp
                            color: (opacity = 1) => `rgba(255, 255, 255, ${opacity})`,
                            labelColor: (opacity = 1) => `rgba(255, 255, 255, ${opacity})`,
                            propsForVerticalLabels: {
                                fontSize: bdpSize,
                                fontFamily: "roboto-regular",
                            },
                            style: {
                                borderRadius: 16
                            },
                            propsForDots: {
                                r: "6",
                                strokeWidth: "2",
                                stroke: "#D4CBE5"
                            }
                        }}
                        bezier
                        style={{
                            marginVertical: 8,
                            borderRadius: 16
                        }}
                    />
                </View>

                <View style={styles.container}>
                    <View style={styles.lineStyle} />
                    <Text style={styles.graphText}>Graph shows {indicator3} data</Text>
                    <LineChart
                        data={{
                            labels: years3,
                            datasets: [
                                {
                                    data: data3
                                }
                            ]
                        }}
                        width={Dimensions.get("window").width} // from react-native
                        height={220}
                        yAxisLabel={vsiPred[2]}
                        yAxisSuffix={vsiPo[2]}
                        yAxisInterval={1} // optional, defaults to 1
                        onDataPointClick={({ value, getColor }) =>
                            showMessage({
                                message: `${vsiPred[2]}${value.toFixed(2)}${vsiPo[2]}`,
                                description: "You selected this value",
                                backgroundColor: "#e26a00"
                            })
                        }
                        chartConfig={{
                            backgroundColor: "#e26a00",
                            backgroundGradientFrom: "#fb8c00",
                            backgroundGradientTo: "#ffa726",
                            decimalPlaces: 1, // optional, defaults to 2dp
                            color: (opacity = 1) => `rgba(255, 255, 255, ${opacity})`,
                            labelColor: (opacity = 1) => `rgba(255, 255, 255, ${opacity})`,
                            propsForVerticalLabels: {
                                fontSize: bdpSize,
                                fontFamily: "roboto-regular",
                            },
                            style: {
                                borderRadius: 16
                            },
                            propsForDots: {
                                r: "6",
                                strokeWidth: "2",
                                stroke: "#D4CBE5"
                            }
                        }}
                        bezier
                        style={{
                            marginVertical: 8,
                            borderRadius: 16
                        }}
                    />
                </View>

                <View style={styles.container}>
                    <View style={styles.lineStyle} />
                    <Text style={styles.graphText}>Graph shows {indicator4} data</Text>
                    <LineChart
                        data={{
                            labels: years4,
                            datasets: [
                                {
                                    data: data4
                                }
                            ]
                        }}
                        width={Dimensions.get("window").width} // from react-native
                        height={220}
                        yAxisLabel={vsiPred[3]}
                        yAxisSuffix={vsiPo[3]}
                        yAxisInterval={1} // optional, defaults to 1
                        onDataPointClick={({ value, getColor }) =>
                            showMessage({
                                message: `${vsiPred[3]}${value.toFixed(2)}${vsiPo[3]}`,
                                description: "You selected this value",
                                backgroundColor: "#e26a00"
                            })
                        }
                        chartConfig={{
                            backgroundColor: "#e26a00",
                            backgroundGradientFrom: "#fb8c00",
                            backgroundGradientTo: "#ffa726",
                            decimalPlaces: 1, // optional, defaults to 2dp
                            color: (opacity = 1) => `rgba(255, 255, 255, ${opacity})`,
                            labelColor: (opacity = 1) => `rgba(255, 255, 255, ${opacity})`,
                            propsForVerticalLabels: {
                                fontSize: bdpSize,
                                fontFamily: "roboto-regular",
                            },
                            style: {
                                borderRadius: 16
                            },
                            propsForDots: {
                                r: "6",
                                strokeWidth: "2",
                                stroke: "#D4CBE5"
                            }
                        }}
                        bezier
                        style={{
                            marginVertical: 8,
                            borderRadius: 16
                        }}
                    />
                </View>

                <View style={styles.container}>
                    <View style={styles.lineStyle} />
                    <Text style={styles.graphText}>Graph shows {indicator5} data</Text>
                    <LineChart
                        data={{
                            labels: years5,
                            datasets: [
                                {
                                    data: data5
                                }
                            ]
                        }}
                        width={Dimensions.get("window").width} // from react-native
                        height={220}
                        yAxisLabel={vsiPred[4]}
                        yAxisSuffix={vsiPo[4]}
                        yAxisInterval={1} // optional, defaults to 1
                        onDataPointClick={({ value, getColor }) =>
                            showMessage({
                                message: `${vsiPred[4]}${value.toFixed(2)}${vsiPo[4]}`,
                                description: "You selected this value",
                                backgroundColor: "#e26a00"
                            })
                        }
                        chartConfig={{
                            backgroundColor: "#e26a00",
                            backgroundGradientFrom: "#fb8c00",
                            backgroundGradientTo: "#ffa726",
                            decimalPlaces: 1, // optional, defaults to 2dp
                            color: (opacity = 1) => `rgba(255, 255, 255, ${opacity})`,
                            labelColor: (opacity = 1) => `rgba(255, 255, 255, ${opacity})`,
                            propsForVerticalLabels: {
                                fontSize: bdpSize,
                                fontFamily: "roboto-regular",
                            },
                            style: {
                                borderRadius: 16
                            },
                            propsForDots: {
                                r: "6",
                                strokeWidth: "2",
                                stroke: "#D4CBE5"
                            }
                        }}
                        bezier
                        style={{
                            marginVertical: 8,
                            borderRadius: 16
                        }}
                    />
                </View>

                <View style={styles.containerLast}>
                    <View style={styles.lineStyle} />
                    <Text style={styles.graphText}>Graph shows {indicator6} data</Text>
                    <LineChart
                        data={{
                            labels: years6,
                            datasets: [
                                {
                                    data: data6
                                }
                            ]
                        }}
                        width={Dimensions.get("window").width} // from react-native
                        height={220}
                        yAxisLabel={vsiPred[5]}
                        yAxisSuffix={vsiPo[5]}
                        yAxisInterval={1} // optional, defaults to 1
                        onDataPointClick={({ value, getColor }) =>
                            showMessage({
                                message: `${vsiPred[5]}${value.toFixed(2)}${vsiPo[5]}`,
                                description: "You selected this value",
                                backgroundColor: "#e26a00"
                            })
                        }
                        chartConfig={{
                            backgroundColor: "#e26a00",
                            backgroundGradientFrom: "#fb8c00",
                            backgroundGradientTo: "#ffa726",
                            decimalPlaces: 1, // optional, defaults to 2dp
                            color: (opacity = 1) => `rgba(255, 255, 255, ${opacity})`,
                            labelColor: (opacity = 1) => `rgba(255, 255, 255, ${opacity})`,
                            propsForVerticalLabels: {
                                fontSize: bdpSize,
                                fontFamily: "roboto-regular",
                            },
                            style: {
                                borderRadius: 16
                            },
                            propsForDots: {
                                r: "6",
                                strokeWidth: "2",
                                stroke: "#D4CBE5"
                            }
                        }}
                        bezier
                        style={{
                            marginVertical: 8,
                            borderRadius: 16
                        }}
                    />

                </View>
            </ScrollView>
        </>
    );
}

const styles = StyleSheet.create({
    overMain: {
        backgroundColor: '#0D3B66',
    },
    textMain: {
        color: '#e26a00',
        fontSize: 30,
        fontWeight: 'bold',
    },
    button: {
        alignItems: 'center',
        justifyContent: 'center',
        paddingVertical: 12,
        paddingHorizontal: 32,
        borderRadius: 4,
        elevation: 3,
        backgroundColor: 'black',
        maxWidth: 300,
    },
    containerLast: {
        flex: 1,
        backgroundColor: '#0D3B66',
        alignItems: 'center',
        paddingBottom: 60,
    },
    container: {
        flex: 1,
        backgroundColor: '#0D3B66',
        alignItems: 'center',
        //justifyContent: 'center',
    },
    text: {
        fontSize: 20,
        fontWeight: 'bold',
    },
    naslov: {
        paddingTop: 10,
        paddingBottom: 10,
        fontSize: 30,
        fontWeight: 'bold',
        backgroundColor: '#0D3B66',
        color: '#e26a00',
    },
    graphText: {
        color: '#ffa726',
        fontSize: 17,
    },
    lineStyle: {
        borderWidth: 0.5,
        borderColor: '#ffa726',
        width: 400,
        margin: 10,
    },
    scrollView: {
        backgroundColor: '#0D3B66',
    },
    dropdown: {
        height: 50,
        borderColor: 'gray',
        borderWidth: 0.5,
        borderRadius: 8,
        paddingHorizontal: 8,
    },
    icon: {
        marginRight: 5,
    },
    label: {
        position: 'absolute',
        backgroundColor: 'white',
        left: 22,
        top: 8,
        zIndex: 999,
        paddingHorizontal: 8,
        fontSize: 14,
    },
    placeholderStyle: {
        fontSize: 16,
    },
    selectedTextStyle: {
        fontSize: 16,
    },
    iconStyle: {
        width: 20,
        height: 20,
    },
    inputSearchStyle: {
        height: 40,
        fontSize: 16,
    },
    imageStyle: {
        width: 24,
        height: 24,
    },
    placeholderStyle: {
        fontSize: 16,
    },
    selectedTextStyle: {
        fontSize: 16,
        marginLeft: 8,
    },
    iconStyle: {
        width: 20,
        height: 20,
    },
    inputSearchStyle: {
        height: 40,
        fontSize: 16,
    },
});

export default SignedHome;
