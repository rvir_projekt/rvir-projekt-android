export const all_countries = [
    {
      value: 'AT',
      lable: 'Austria',
      image: {
        uri: 'https://upload.wikimedia.org/wikipedia/commons/thumb/4/41/Flag_of_Austria.svg/255px-Flag_of_Austria.svg.png',
      },
    },
    {
      value: 'AU',
      lable: 'Australia',
      image: {
        uri: 'https://cdn.britannica.com/78/6078-004-77AF7322/Flag-Australia.jpg',
      },
    },
    {
      value: 'BE',
      lable: 'Belgium',
      image: {
        uri: 'https://upload.wikimedia.org/wikipedia/commons/thumb/6/65/Flag_of_Belgium.svg/2363px-Flag_of_Belgium.svg.png',
      },
    },
    {
      value: 'CA',
      lable: 'Canada',
      image: {
        uri: 'https://upload.wikimedia.org/wikipedia/commons/d/d9/Flag_of_Canada_%28Pantone%29.svg',
      },
    },
    {
      value: 'US',
      lable: 'USA',
      image: {
        uri: 'https://upload.wikimedia.org/wikipedia/en/thumb/a/a4/Flag_of_the_United_States.svg/255px-Flag_of_the_United_States.svg.png',
      },
    },
  ];