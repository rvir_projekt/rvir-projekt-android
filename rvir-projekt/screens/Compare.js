
import { StyleSheet, Text, View, Image, Button, Dimensions, ScrollView, Pressable } from 'react-native';
import 'expo-dev-client';
import { GoogleSignin, GoogleSigninButton } from '@react-native-google-signin/google-signin';
import auth from '@react-native-firebase/auth';
import React, { useState, useEffect } from 'react';
import { LineChart, BarChart, PieChart, ProgressChart, ContributionGraph, StackedBarChart } from "react-native-chart-kit";
import { getLocales } from 'expo-localization';
import DeviceCountry, { TYPE_ANY, TYPE_TELEPHONY, TYPE_CONFIGURATION } from 'react-native-device-country';
import FlashMessage, { showMessage } from "react-native-flash-message";
import { Overlay, Icon } from '@rneui/themed';
import { Dropdown, SelectCountry } from 'react-native-element-dropdown';
import Ionicons from '@expo/vector-icons/Ionicons';
import { all_countries } from '../data/countries';
import { numbers } from '../data/numbers';

const data = [
    { label: 'GDP (current US$)', value: 'NY.GDP.MKTP.CD' },
    { label: 'GDP growth (annual %)', value: 'NY.GDP.MKTP.KD.ZG' },
    { label: 'GDP per capita (current US$)', value: 'NY.GDP.PCAP.CD' },
    { label: 'Inflation (annual %)', value: 'NY.GDP.DEFL.KD.ZG' },
    { label: 'Unemployment, total (% of total labor force)', value: 'SL.UEM.TOTL.NE.ZS' },
    { label: 'Population, total', value: 'SP.POP.TOTL' },
    { label: 'Population growth (annual %)', value: 'SP.POP.GROW' },
    { label: 'Agricultural land (% of land area)', value: 'AG.LND.AGRI.ZS' },
    { label: 'Forest area (% of land area)', value: 'AG.LND.FRST.ZS' },
    { label: 'CO2 emissions (metric tons per capita)', value: 'EN.ATM.CO2E.PC' },
    { label: 'Life expectancy at birth, total (years)', value: 'SP.DYN.LE00.IN' },
    { label: 'Individuals using the Internet (% of population)', value: 'IT.NET.USER.ZS' },
    { label: 'Air transport, passengers carried', value: 'IS.AIR.PSGR' },
    //{ label: '', value: '' },
    //{ label: '', value: '' },
    //{ label: '', value: '' },
];

const Compare = () => {
    const [initializing, setInitializing] = useState(true);
    const [user, setUser] = useState();
    const [data1, setData1] = useState([1, 1, 1, 1]);
    const [data2, setData2] = useState([1, 1, 1, 1]);
    const [years1, setYears1] = useState([2019, 2020, 2021, 2022]);
    const [bdpSize, setBdpSize] = useState(15);
    const [country1, setCountry1] = useState('US');
    const [countryName1, setCountryName1] = useState('USA');
    const [country2, setCountry2] = useState('US');
    const [countryName2, setCountryName2] = useState('USA');
    const [indicators, setIndicators] = useState([data[0].value, data[0].value, data[0].value, data[0].value, data[0].value, data[0].value]);
    const [indicator1, setIndicator1] = useState('');
    const [indicator2, setIndicator2] = useState('');
    const [indicator3, setIndicator3] = useState('');
    const [indicator4, setIndicator4] = useState('');
    const [indicator5, setIndicator5] = useState('');
    const [indicator6, setIndicator6] = useState('');
    const [vsiPred, setVsiPred] = useState(['', '', '', '', '', '']);
    const [vsiPo, setVsiPo] = useState(['', '', '', '', '', '']);
    const [numberValue, setNumberValue] = useState(5);
    const [visible, setVisible] = useState(false);
    const [value1, setValue1] = useState(null);
    const [value2, setValue2] = useState(null);
    const [value3, setValue3] = useState(null);
    const [value4, setValue4] = useState(null);
    const [value5, setValue5] = useState(null);
    const [value6, setValue6] = useState(null);
    const [isFocus, setIsFocus] = useState(false);

    const toggleOverlay = () => {
        setVisible(!visible);
    };

    const makeAPICall = async (arr) => {
        try {
            let country_ = country1;
            let country_2 = country2;
            const response = await fetch(
                'https://api.worldbank.org/v2/en/country/' + country_ + '/indicator/' + arr[0] + '?mrv=' + numberValue + '&format=json', //NY.GDP.MKTP.CD
            );

            const response2 = await fetch(
                'https://api.worldbank.org/v2/en/country/' + country_2 + '/indicator/' + arr[0] + '?mrv=' + numberValue + '&format=json', //NY.GDP.MKTP.CD
            );
            const json = await response.json();
            const json2 = await response2.json();
            console.log(json[1][0].country.value);
            console.log(json2[1][0].country.value);
            if (countryName1 != json[1][0].country.value) {
                setCountryName1(json[1][0].country.value);
            }

            if (countryName2 != json2[1][0].country.value) {
                setCountryName2(json2[1][0].country.value);
            }
            const len = json[1].length;
            if (len >= 9 && len < 10) {
                setBdpSize(13);
            } else if (len >= 10 && len < 16) {
                setBdpSize(9);
            } else if (len >= 16 && len < 21) {
                setBdpSize(7);
            } else if (len >= 21 && len < 25) {
                setBdpSize(6);
            } else if (len >= 25) {
                setBdpSize(5);
            } else {
                setBdpSize(15);
            }
            let bData = [];
            let secData = [];
            let allYears = [];

            let largest = 0;
            for (let i = 0; i < len; i++) {
                if (json[1][i].value > largest) {
                    largest = json[1][i].value;
                }

                if (json2[1][i].value > largest) {
                    largest = json2[1][i].value;
                }
                //bData.push(json[1][i].value / 1000000000);
                //allYears.push(json[1][i].date);
            }

            let pred = '';
            let po = '';
            if (largest > 1000000000) {
                for (let i = 0; i < len; i++) {
                    bData.push(json[1][i].value / 1000000000);
                    secData.push(json2[1][i].value / 1000000000);
                    allYears.push(json[1][i].date);
                }
                po = 'B';
            } else if (largest > 1000000) {
                for (let i = 0; i < len; i++) {
                    bData.push(json[1][i].value / 1000000);
                    secData.push(json2[1][i].value / 1000000);
                    allYears.push(json[1][i].date);
                }
                po = 'M';
            } else if (largest > 1000) {
                for (let i = 0; i < len; i++) {
                    bData.push(json[1][i].value / 1000);
                    secData.push(json2[1][i].value / 1000);
                    allYears.push(json[1][i].date);
                }
                po = 'T';
            } else {
                for (let i = 0; i < len; i++) {
                    bData.push(json[1][i].value);
                    secData.push(json2[1][i].value);
                    allYears.push(json[1][i].date);
                }
            }

            secData.reverse();
            bData.reverse();
            allYears.reverse();

            if (json[1][0].indicator.value.includes('$')) {
                pred = '$';
            } else if (json[1][0].indicator.value.includes('%')) {
                po = '%';
            }

            setIndicator1(json[1][0].indicator.value);
            setData2(secData);
            setData1(bData);
            setYears1(allYears);
            setVsiPred(pred);
            setVsiPo(po);

            console.log(data1);
            console.log(data2);

            //return json;
        } catch (error) {
            console.error(error);
        }
    };

    useEffect(() => {
        setCountry1('US');
    }, []);

    useEffect(() => {
        makeAPICall(indicators);
    }, [country1, country2, indicators, value1, numberValue, value2, value3, value4, value5, value6]);

    return (
        <>
            <View style={{ alignItems: 'center', backgroundColor: '#0D3B66' }}>
                <Text style={styles.naslov}>Showing results for {countryName1} and {countryName2}</Text>
            </View>

            <View style={{ backgroundColor: '#0D3B66', alignItems: 'center', paddingBottom: 7 }}>
                <Button
                    title="Modify graphs"
                    onPress={toggleOverlay}
                    buttonStyle={styles.button}
                />
            </View>
            <Overlay overlayStyle={styles.overMain} isVisible={visible} onBackdropPress={toggleOverlay}>
                <View style={{ alignItems: 'center' }}>
                    <Text style={styles.textMain}>
                        Modify graphs
                    </Text>
                </View>

                <Text style={styles.textSelector}>
                    Choose how many years you want the graph so show
                </Text>

                <Dropdown
                    style={[styles.dropdown, isFocus && { borderColor: 'blue' }]}
                    placeholderStyle={styles.placeholderStyle}
                    selectedTextStyle={styles.selectedTextStyle}
                    inputSearchStyle={styles.inputSearchStyle}
                    iconStyle={styles.iconStyle}
                    data={numbers}
                    search
                    maxHeight={300}
                    labelField="label"
                    valueField="value"
                    placeholder={!isFocus ? 'Select number' : '...'}
                    searchPlaceholder="Search..."
                    value={value1}
                    onFocus={() => setIsFocus(true)}
                    onBlur={() => setIsFocus(false)}
                    onChange={item => {
                        setNumberValue(item.value);
                        setIsFocus(false);
                    }}
                    renderLeftIcon={() => (
                        <Ionicons name={'keypad'} size={20} color={isFocus ? 'blue' : 'black'} />

                    )}
                />

                <Text style={styles.textSelector}>
                    Select country 1
                </Text>

                <SelectCountry
                    style={styles.dropdown}
                    selectedTextStyle={styles.selectedTextStyle}
                    placeholderStyle={styles.placeholderStyle}
                    imageStyle={styles.imageStyle}
                    inputSearchStyle={styles.inputSearchStyle}
                    iconStyle={styles.iconStyle}
                    search
                    maxHeight={200}
                    value={country1}
                    data={all_countries}
                    valueField="value"
                    labelField="lable"
                    imageField="image"
                    placeholder="Select country"
                    searchPlaceholder="Search..."
                    onChange={e => {
                        setCountry1(e.value);
                    }}
                />

                <Text style={styles.textSelector}>
                    Select country 2
                </Text>

                <SelectCountry
                    style={styles.dropdown}
                    selectedTextStyle={styles.selectedTextStyle}
                    placeholderStyle={styles.placeholderStyle}
                    imageStyle={styles.imageStyle}
                    inputSearchStyle={styles.inputSearchStyle}
                    iconStyle={styles.iconStyle}
                    search
                    maxHeight={200}
                    value={country2}
                    data={all_countries}
                    valueField="value"
                    labelField="lable"
                    imageField="image"
                    placeholder="Select country"
                    searchPlaceholder="Search..."
                    onChange={e => {
                        setCountry2(e.value);
                    }}
                />

                <Text style={styles.textSelector}>
                    Select indicator for graph 1
                </Text>

                <Dropdown
                    style={[styles.dropdown, isFocus && { borderColor: 'blue' }]}
                    placeholderStyle={styles.placeholderStyle}
                    selectedTextStyle={styles.selectedTextStyle}
                    inputSearchStyle={styles.inputSearchStyle}
                    iconStyle={styles.iconStyle}
                    data={data}
                    search
                    maxHeight={300}
                    labelField="label"
                    valueField="value"
                    placeholder={!isFocus ? 'Select indicator' : '...'}
                    searchPlaceholder="Search..."
                    value={value1}
                    onFocus={() => setIsFocus(true)}
                    onBlur={() => setIsFocus(false)}
                    onChange={item => {
                        setValue1(item.value);
                        setIsFocus(false);
                        let all = indicators;
                        all[0] = item.value;
                        setIndicators(all);
                    }}
                    renderLeftIcon={() => (
                        <Ionicons name={'bar-chart-outline'} size={20} color={isFocus ? 'blue' : 'black'} />

                    )}
                />

                <Button
                    icon={
                        <Icon
                            name="wrench"
                            type="font-awesome"
                            color="white"
                            size={25}
                            iconStyle={{ marginRight: 10 }}
                        />
                    }
                    title="Confirm"
                    onPress={toggleOverlay}
                />
            </Overlay>


            <FlashMessage duration={1000} />
            <ScrollView style={styles.scrollView}>

                <View style={styles.container}>
                    <View style={styles.lineStyle} />
                    <Text style={styles.graphText}>Graph compares {indicator1} data</Text>
                    <LineChart
                        data={{
                            labels: years1,
                            datasets: [
                                { data: data1, color: () => '#C7EBFF', strokeWidth: 4 }, { data: data2, color: () => '#ED7C33' },
                            ]
                        }}
                        width={Dimensions.get("window").width} // from react-native
                        height={220}
                        yAxisLabel={vsiPred[0]}
                        yAxisSuffix={vsiPo[0]}
                        yAxisInterval={1} // optional, defaults to 1
                        onDataPointClick={({ value, getColor }) =>
                            showMessage({
                                message: `${vsiPred[0]}${value.toFixed(2)}${vsiPo[0]}`,
                                description: "You selected this value",
                                backgroundColor: "#e26a00"
                            })
                        }
                        chartConfig={{
                            backgroundColor: "#e26a00",
                            backgroundGradientFrom: "#fb8c00",
                            backgroundGradientTo: "#ffa726",
                            decimalPlaces: 1, // optional, defaults to 2dp
                            color: (opacity = 1) => `rgba(255, 255, 255, ${opacity})`,
                            labelColor: (opacity = 1) => `rgba(255, 255, 255, ${opacity})`,
                            propsForVerticalLabels: {
                                fontSize: bdpSize,
                                fontFamily: "roboto-regular",
                            },
                            style: {
                                borderRadius: 16
                            },
                            propsForDots: {
                                r: "6",
                                strokeWidth: "2",
                                stroke: "#D4CBE5"
                            }
                        }}
                        bezier
                        style={{
                            marginVertical: 8,
                            borderRadius: 16
                        }}
                    />
                    <Text style={styles.naslov}>Legend for lines in the chart:</Text>
                    <Text style={styles.graphText}><Text style={{color:"#C7EBFF"}}>{countryName1}</Text></Text>
                    <Text style={styles.graphText}>{countryName2}</Text>
                </View>


            </ScrollView>
        </>
    );
}

const styles = StyleSheet.create({
    overMain: {
        backgroundColor: '#0D3B66',
    },
    textMain: {
        color: '#e26a00',
        fontSize: 30,
        fontWeight: 'bold',
    },
    button: {
        alignItems: 'center',
        justifyContent: 'center',
        paddingVertical: 12,
        paddingHorizontal: 32,
        borderRadius: 4,
        elevation: 3,
        backgroundColor: 'black',
        maxWidth: 300,
    },
    containerLast: {
        flex: 1,
        backgroundColor: '#0D3B66',
        alignItems: 'center',
        paddingBottom: 60,
    },
    container: {
        flex: 1,
        backgroundColor: '#0D3B66',
        alignItems: 'center',
        //justifyContent: 'center',
    },
    text: {
        fontSize: 20,
        fontWeight: 'bold',
    },
    naslov: {
        paddingTop: 10,
        paddingBottom: 10,
        fontSize: 30,
        fontWeight: 'bold',
        backgroundColor: '#0D3B66',
        color: '#e26a00',
    },
    graphText: {
        color: '#ffa726',
        fontSize: 17,
    },
    lineStyle: {
        borderWidth: 0.5,
        borderColor: '#ffa726',
        width: 400,
        margin: 10,
    },
    scrollView: {
        backgroundColor: '#0D3B66',
    },
    dropdown: {
        height: 50,
        borderColor: 'gray',
        borderWidth: 0.5,
        borderRadius: 8,
        paddingHorizontal: 8,
    },
    icon: {
        marginRight: 5,
    },
    label: {
        position: 'absolute',
        backgroundColor: 'white',
        left: 22,
        top: 8,
        zIndex: 999,
        paddingHorizontal: 8,
        fontSize: 14,
    },
    placeholderStyle: {
        fontSize: 16,
    },
    selectedTextStyle: {
        fontSize: 16,
    },
    iconStyle: {
        width: 20,
        height: 20,
    },
    inputSearchStyle: {
        height: 40,
        fontSize: 16,
    },
    imageStyle: {
        width: 24,
        height: 24,
    },
    placeholderStyle: {
        fontSize: 16,
    },
    selectedTextStyle: {
        fontSize: 16,
        marginLeft: 8,
    },
    iconStyle: {
        width: 20,
        height: 20,
    },
    inputSearchStyle: {
        height: 40,
        fontSize: 16,
    },
});

export default Compare;
