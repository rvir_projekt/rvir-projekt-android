import { StatusBar } from 'expo-status-bar';
import { StyleSheet, Text, View, Image, Button, Dimensions, ScrollView } from 'react-native';
import 'expo-dev-client';
import { GoogleSignin, GoogleSigninButton } from '@react-native-google-signin/google-signin';
import auth from '@react-native-firebase/auth';
import React, { useState, useEffect } from 'react';
import { LineChart, BarChart, PieChart, ProgressChart, ContributionGraph, StackedBarChart } from "react-native-chart-kit";
import { getLocales } from 'expo-localization';
import DeviceCountry, { TYPE_ANY, TYPE_TELEPHONY, TYPE_CONFIGURATION } from 'react-native-device-country';
import FlashMessage, { showMessage } from "react-native-flash-message";
import {useNetInfo} from "@react-native-community/netinfo";
import AsyncStorage from '@react-native-async-storage/async-storage';

import Header from '../components/Header';
import SignedHome from '../components/SignedHome';

export default function Home() {
  // Set an initializing state whilst Firebase connects
  const [initializing, setInitializing] = useState(true);
  const [user, setUser] = useState();
  const [bdpData, setBdpData] = useState([0, 0, 0, 0]);
  const [popData, setPopData] = useState([0, 0, 0, 0]);
  const [unemData, setUnemData] = useState([0, 0, 0, 0]);
  const [years, setYears] = useState([2019, 2020, 2021, 2022]);
  const [bdpSize, setBdpSize] = useState(15);
  const [country, setCountry] = useState('');
  const [indicator1, setIndicator1] = useState('');
  const [indicator2, setIndicator2] = useState('');
  const [indicator3, setIndicator3] = useState('');
  GoogleSignin.configure({
    webClientId: '618840812696-60vga9dub93foc3ll8g1ihokn6jkd9bg.apps.googleusercontent.com',
  });
  const netInfo = useNetInfo();

  // Handle user state changes
  function onAuthStateChanged(user) {
    setUser(user);
    if (initializing) setInitializing(false);
  }

  const makeAPICall = async (indic) => {
    try {
      let country = 'usa';
      await DeviceCountry.getCountryCode(TYPE_ANY)
        .then((result) => {
          country = result.code;
          // {"code": "BY", "type": "telephony"}
        })
        .catch((e) => {
          console.log(e);
        });
      let response;
      let json;
      if (netInfo.isConnected) {
        response = await fetch(
          'https://api.worldbank.org/v2/en/country/' + country + '/indicator/'+ indic +'?mrv=8&format=json', //NY.GDP.MKTP.CD
        );
        json = await response.json();
      }else{
        json = await AsyncStorage.getItem('@response');
        json = JSON.parse(json);
        console.log(json);
      }
      await AsyncStorage.setItem('@response', JSON.stringify(json))
      setCountry(json[1][0].country.value);
      const len = json[1].length;
      if (len >= 9 && len < 10) {
        setBdpSize(13);
      } else if (len >= 10 && len < 16) {
        setBdpSize(9);
      } else if (len >= 16 && len < 21) {
        setBdpSize(7);
      } else if (len >= 21 && len < 25) {
        setBdpSize(6);
      } else if (len >= 25) {
        setBdpSize(5);
      } else {
        setBdpSize(15);
      }
      let bData = [];
      let allYears = [];
      allYears.reverse();
      if (indic == 'NY.GDP.MKTP.CD') {
        for (let i = 0; i < len; i++) {
          bData.push(json[1][i].value / 1000000000);
          allYears.push(json[1][i].date);
        }
        allYears.reverse();
        bData.reverse();
        setIndicator1(json[1][0].indicator.value);
        setBdpData(bData);
        setYears(allYears);
      } else if (indic == 'SP.POP.TOTL') {
        for (let i = 0; i < len; i++) {
          bData.push(json[1][i].value / 1000000);
          allYears.push(json[1][i].date);
        }
        allYears.reverse();
        bData.reverse();
        setIndicator2(json[1][0].indicator.value);
        setPopData(bData);
        setYears(allYears);
      } else if (indic == 'SL.UEM.TOTL.NE.ZS') {
        for (let i = 0; i < len; i++) {
          bData.push(json[1][i].value);
          allYears.push(json[1][i].date);
        }
        allYears.reverse();
        bData.reverse();
        setIndicator3(json[1][0].indicator.value);
        setUnemData(bData);
        setYears(allYears);
      }

      let re = await AsyncStorage.getItem('@response');
      console.log(re);

      return json;
    } catch (error) {
      console.error(error);
    }
  };

  useEffect(() => {
    makeAPICall('NY.GDP.MKTP.CD');
    makeAPICall('SP.POP.TOTL');
    makeAPICall('SL.UEM.TOTL.NE.ZS');
  }, []);

  useEffect(() => {
    const subscriber = auth().onAuthStateChanged(onAuthStateChanged);
    return subscriber; // unsubscribe on unmount
  }, []);
  if (initializing) return null;

  if (!user) {
    return (
      <>
        <View style={{ alignItems: 'center', backgroundColor: '#0D3B66' }}>
          <Text style={styles.naslov}>Showing results for {country}</Text>
        </View>
        <FlashMessage duration={1000} />
        <ScrollView style={styles.scrollView}>

          <View style={styles.container}>
            <View style={styles.lineStyle} />
            <Text style={styles.graphText}>Graph shows {indicator1} data</Text>
            <LineChart
              data={{
                labels: years,
                datasets: [
                  {
                    data: bdpData
                  }
                ]
              }}
              width={Dimensions.get("window").width} // from react-native
              height={220}
              yAxisLabel="$"
              yAxisSuffix="B"
              yAxisInterval={1} // optional, defaults to 1
              onDataPointClick={({ value, getColor }) =>
                showMessage({
                  message: `$${value.toFixed(2)}B`,
                  description: "You selected this value",
                  backgroundColor: "#e26a00"
                })
              }
              chartConfig={{
                backgroundColor: "#e26a00",
                backgroundGradientFrom: "#fb8c00",
                backgroundGradientTo: "#ffa726",
                decimalPlaces: 1, // optional, defaults to 2dp
                color: (opacity = 1) => `rgba(255, 255, 255, ${opacity})`,
                labelColor: (opacity = 1) => `rgba(255, 255, 255, ${opacity})`,
                propsForVerticalLabels: {
                  fontSize: bdpSize,
                  fontFamily: "roboto-regular",
                },
                style: {
                  borderRadius: 16
                },
                propsForDots: {
                  r: "6",
                  strokeWidth: "2",
                  stroke: "#D4CBE5"
                }
              }}
              bezier
              style={{
                marginVertical: 8,
                borderRadius: 16
              }}
            />
          </View>

          <View style={styles.container}>
            <View style={styles.lineStyle} />
            <Text style={styles.graphText}>Graph shows {indicator2} data</Text>
            <LineChart
              data={{
                labels: years,
                datasets: [
                  {
                    data: popData
                  }
                ]
              }}
              width={Dimensions.get("window").width} // from react-native
              height={220}
              yAxisLabel=""
              yAxisSuffix="M"
              yAxisInterval={1} // optional, defaults to 1
              onDataPointClick={({ value, getColor }) =>
                showMessage({
                  message: `${value.toFixed(2)}M`,
                  description: "You selected this value",
                  backgroundColor: "#e26a00"
                })
              }
              chartConfig={{
                backgroundColor: "#e26a00",
                backgroundGradientFrom: "#fb8c00",
                backgroundGradientTo: "#ffa726",
                decimalPlaces: 1, // optional, defaults to 2dp
                color: (opacity = 1) => `rgba(255, 255, 255, ${opacity})`,
                labelColor: (opacity = 1) => `rgba(255, 255, 255, ${opacity})`,
                propsForVerticalLabels: {
                  fontSize: bdpSize,
                  fontFamily: "roboto-regular",
                },
                style: {
                  borderRadius: 16
                },
                propsForDots: {
                  r: "6",
                  strokeWidth: "2",
                  stroke: "#D4CBE5"
                }
              }}
              bezier
              style={{
                marginVertical: 8,
                borderRadius: 16
              }}
            />
          </View>

          <View style={styles.containerLast}>
            <View style={styles.lineStyle} />
            <Text style={styles.graphText}>Graph shows {indicator3} data</Text>
            <LineChart
              data={{
                labels: years,
                datasets: [
                  {
                    data: unemData
                  }
                ]
              }}
              width={Dimensions.get("window").width} // from react-native
              height={220}
              yAxisLabel=""
              yAxisSuffix="%"
              yAxisInterval={1} // optional, defaults to 1
              onDataPointClick={({ value, getColor }) =>
                showMessage({
                  message: `${value.toFixed(2)}%`,
                  description: "You selected this value",
                  backgroundColor: "#e26a00"
                })
              }
              chartConfig={{
                backgroundColor: "#e26a00",
                backgroundGradientFrom: "#fb8c00",
                backgroundGradientTo: "#ffa726",
                decimalPlaces: 1, // optional, defaults to 2dp
                color: (opacity = 1) => `rgba(255, 255, 255, ${opacity})`,
                labelColor: (opacity = 1) => `rgba(255, 255, 255, ${opacity})`,
                propsForVerticalLabels: {
                  fontSize: bdpSize,
                  fontFamily: "roboto-regular",
                },
                style: {
                  borderRadius: 16
                },
                propsForDots: {
                  r: "6",
                  strokeWidth: "2",
                  stroke: "#D4CBE5"
                }
              }}
              bezier
              style={{
                marginVertical: 8,
                borderRadius: 16
              }}
            />
            
          </View>
        </ScrollView>
      </>
    );
  }

  return (
    <>
      <SignedHome></SignedHome>
    </>
  );
}

const styles = StyleSheet.create({
  containerLast: {
    flex: 1,
    backgroundColor: '#0D3B66',
    alignItems: 'center',
    paddingBottom: 60,
  },
  container: {
    flex: 1,
    backgroundColor: '#0D3B66',
    alignItems: 'center',
    //justifyContent: 'center',
  },
  text: {
    fontSize: 20,
    fontWeight: 'bold',
  },
  naslov: {
    paddingTop: 10,
    paddingBottom: 10,
    fontSize: 30,
    fontWeight: 'bold',
    backgroundColor: '#0D3B66',
    color: '#e26a00',
  },
  graphText: {
    color: '#ffa726',
    fontSize: 17,
  },
  lineStyle: {
    borderWidth: 0.5,
    borderColor: '#ffa726',
    width: 400,
    margin: 10,
  },
  scrollView: {
    backgroundColor: '#0D3B66',
  }
});
